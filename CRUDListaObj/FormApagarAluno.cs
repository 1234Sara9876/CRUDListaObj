﻿

namespace CRUDListaObj
{
    using System.Collections.Generic;
    using Modelos;
    using System.Windows.Forms;
    public partial class FormApagarAluno : Form
    {
        private List<Aluno> ListaDeAlunos;
       
        private Aluno alunoAApagar;

        public FormApagarAluno( List<Aluno>listaAlunos,Aluno aluno)
        {
            InitializeComponent();
            this.ListaDeAlunos = listaAlunos;
            alunoAApagar = aluno;
            DataGridViewAlunoApagar.Rows.Add(alunoAApagar.IdAluno, alunoAApagar.PrimeiroNome, alunoAApagar.Apelido);
            DataGridViewAlunoApagar.AllowUserToAddRows = false;
        }

        private void ButtonApagar_Click(object sender, System.EventArgs e)
        {
            int index = 0;
            foreach(var aluno in ListaDeAlunos)
            {
                if(alunoAApagar.IdAluno == aluno.IdAluno)
                {
                    ListaDeAlunos.RemoveAt(index);
                    break;
                }
                index++;
            }
            MessageBox.Show("Aluno apagado");
           
            Close();
        }

        private void ButtonCancelar_Click(object sender, System.EventArgs e)
        {
            MessageBox.Show("O aluno não foi apagado");
            Close();
        }

        private void FormApagarAluno_Load(object sender, System.EventArgs e)
        {

        }
    }
}
