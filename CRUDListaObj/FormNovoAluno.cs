﻿
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUDListaObj
{
    using Modelos;
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;

    public partial class FormNovoAluno : Form
    {
        private List<Aluno> listaDeAlunos = new List<Aluno>();

        public FormNovoAluno(List<Aluno> listaDeAlunos)//o q está entre parêntesis é uma referência
        {
            InitializeComponent();
            this.listaDeAlunos = listaDeAlunos;//diz que n é uma lista nova, mas uma referência da outra
        }

        private void ButtonNovoAluno_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(TextBoxPrimeiroNome.Text))
            {
                MessageBox.Show("Tem que inserir o primeiro nome do aluno");
                return;
            }
            if (string.IsNullOrEmpty(TextBoxPrimeiroNome.Text))
            {
                MessageBox.Show("Tem que inserir o apelido do aluno");
                return;
            }

            var aluno = new Aluno { IdAluno = GeraIdAluno(), PrimeiroNome = TextBoxPrimeiroNome.Text, Apelido = TextBoxApelido.Text };
            listaDeAlunos.Add(aluno);
            Close();
        }
        private int GeraIdAluno()
        {
            return listaDeAlunos[listaDeAlunos.Count - 1].IdAluno + 1;
        }

        private void FormNovoAluno_Load(object sender, EventArgs e)
        {

        }
    }
}
