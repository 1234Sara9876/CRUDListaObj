﻿namespace CRUDListaObj
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.ComboBoxNomes = new System.Windows.Forms.ComboBox();
            this.ComboBoxListaAlunos = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.ButtonProcurar = new System.Windows.Forms.Button();
            this.TextBoxProcurar = new System.Windows.Forms.TextBox();
            this.ComboBoxProcurar = new System.Windows.Forms.ComboBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.TextBoxApelidoAluno = new System.Windows.Forms.TextBox();
            this.TextBoxNomeAluno = new System.Windows.Forms.TextBox();
            this.TextBoxPrimeiroNome = new System.Windows.Forms.TextBox();
            this.TextBoxIdAluno = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.alunoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.novoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.apagarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.actualizarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ButtonCancelar = new System.Windows.Forms.Button();
            this.ButtonGravar = new System.Windows.Forms.Button();
            this.ButtonEditar = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.ComboBoxNomes);
            this.groupBox1.Controls.Add(this.ComboBoxListaAlunos);
            this.groupBox1.Location = new System.Drawing.Point(18, 92);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(329, 127);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Listagens";
            // 
            // groupBox3
            // 
            this.groupBox3.Location = new System.Drawing.Point(0, 157);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(295, 81);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "groupBox3";
            // 
            // ComboBoxNomes
            // 
            this.ComboBoxNomes.FormattingEnabled = true;
            this.ComboBoxNomes.Location = new System.Drawing.Point(31, 83);
            this.ComboBoxNomes.Name = "ComboBoxNomes";
            this.ComboBoxNomes.Size = new System.Drawing.Size(221, 21);
            this.ComboBoxNomes.TabIndex = 1;
            // 
            // ComboBoxListaAlunos
            // 
            this.ComboBoxListaAlunos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxListaAlunos.FormattingEnabled = true;
            this.ComboBoxListaAlunos.Location = new System.Drawing.Point(31, 37);
            this.ComboBoxListaAlunos.Name = "ComboBoxListaAlunos";
            this.ComboBoxListaAlunos.Size = new System.Drawing.Size(264, 21);
            this.ComboBoxListaAlunos.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.ButtonProcurar);
            this.groupBox2.Controls.Add(this.TextBoxProcurar);
            this.groupBox2.Controls.Add(this.ComboBoxProcurar);
            this.groupBox2.Location = new System.Drawing.Point(396, 92);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(336, 127);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Procurar";
            // 
            // ButtonProcurar
            // 
            this.ButtonProcurar.Location = new System.Drawing.Point(125, 83);
            this.ButtonProcurar.Name = "ButtonProcurar";
            this.ButtonProcurar.Size = new System.Drawing.Size(75, 23);
            this.ButtonProcurar.TabIndex = 2;
            this.ButtonProcurar.Text = "Procurar";
            this.ButtonProcurar.UseVisualStyleBackColor = true;
            this.ButtonProcurar.Click += new System.EventHandler(this.ButtonProcurar_Click);
            // 
            // TextBoxProcurar
            // 
            this.TextBoxProcurar.Location = new System.Drawing.Point(169, 37);
            this.TextBoxProcurar.Name = "TextBoxProcurar";
            this.TextBoxProcurar.Size = new System.Drawing.Size(146, 20);
            this.TextBoxProcurar.TabIndex = 1;
            // 
            // ComboBoxProcurar
            // 
            this.ComboBoxProcurar.FormattingEnabled = true;
            this.ComboBoxProcurar.Items.AddRange(new object[] {
            "Id Aluno",
            "Nome"});
            this.ComboBoxProcurar.Location = new System.Drawing.Point(30, 37);
            this.ComboBoxProcurar.Name = "ComboBoxProcurar";
            this.ComboBoxProcurar.Size = new System.Drawing.Size(121, 21);
            this.ComboBoxProcurar.TabIndex = 0;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Controls.Add(this.ButtonCancelar);
            this.groupBox4.Controls.Add(this.ButtonGravar);
            this.groupBox4.Controls.Add(this.ButtonEditar);
            this.groupBox4.Controls.Add(this.TextBoxApelidoAluno);
            this.groupBox4.Controls.Add(this.TextBoxNomeAluno);
            this.groupBox4.Controls.Add(this.TextBoxPrimeiroNome);
            this.groupBox4.Controls.Add(this.TextBoxIdAluno);
            this.groupBox4.Location = new System.Drawing.Point(18, 225);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(714, 105);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Editar";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(254, 54);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Apelido";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(253, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Nome Completo";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(36, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Nome";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(36, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "ID Aluno";
            // 
            // TextBoxApelidoAluno
            // 
            this.TextBoxApelidoAluno.Location = new System.Drawing.Point(378, 51);
            this.TextBoxApelidoAluno.Name = "TextBoxApelidoAluno";
            this.TextBoxApelidoAluno.Size = new System.Drawing.Size(100, 20);
            this.TextBoxApelidoAluno.TabIndex = 3;
            // 
            // TextBoxNomeAluno
            // 
            this.TextBoxNomeAluno.Location = new System.Drawing.Point(378, 18);
            this.TextBoxNomeAluno.Name = "TextBoxNomeAluno";
            this.TextBoxNomeAluno.ReadOnly = true;
            this.TextBoxNomeAluno.Size = new System.Drawing.Size(100, 20);
            this.TextBoxNomeAluno.TabIndex = 2;
            // 
            // TextBoxPrimeiroNome
            // 
            this.TextBoxPrimeiroNome.Location = new System.Drawing.Point(119, 54);
            this.TextBoxPrimeiroNome.Name = "TextBoxPrimeiroNome";
            this.TextBoxPrimeiroNome.Size = new System.Drawing.Size(100, 20);
            this.TextBoxPrimeiroNome.TabIndex = 1;
            // 
            // TextBoxIdAluno
            // 
            this.TextBoxIdAluno.Location = new System.Drawing.Point(119, 18);
            this.TextBoxIdAluno.Name = "TextBoxIdAluno";
            this.TextBoxIdAluno.ReadOnly = true;
            this.TextBoxIdAluno.Size = new System.Drawing.Size(100, 20);
            this.TextBoxIdAluno.TabIndex = 0;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.alunoToolStripMenuItem,
            this.actualizarToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(744, 24);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // alunoToolStripMenuItem
            // 
            this.alunoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.novoToolStripMenuItem,
            this.apagarToolStripMenuItem});
            this.alunoToolStripMenuItem.Name = "alunoToolStripMenuItem";
            this.alunoToolStripMenuItem.Size = new System.Drawing.Size(51, 20);
            this.alunoToolStripMenuItem.Text = "Aluno";
            // 
            // novoToolStripMenuItem
            // 
            this.novoToolStripMenuItem.Name = "novoToolStripMenuItem";
            this.novoToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.novoToolStripMenuItem.Text = "Novo";
            this.novoToolStripMenuItem.Click += new System.EventHandler(this.alunoToolStripMenuItem_Click);
            // 
            // apagarToolStripMenuItem
            // 
            this.apagarToolStripMenuItem.Name = "apagarToolStripMenuItem";
            this.apagarToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.apagarToolStripMenuItem.Text = "Apagar";
            this.apagarToolStripMenuItem.Click += new System.EventHandler(this.apagarToolStripMenuItem_Click);
            // 
            // actualizarToolStripMenuItem
            // 
            this.actualizarToolStripMenuItem.Name = "actualizarToolStripMenuItem";
            this.actualizarToolStripMenuItem.Size = new System.Drawing.Size(71, 20);
            this.actualizarToolStripMenuItem.Text = "Actualizar";
            this.actualizarToolStripMenuItem.Click += new System.EventHandler(this.actualizarToolStripMenuItem_Click);
            // 
            // ButtonCancelar
            // 
            this.ButtonCancelar.Enabled = false;
            this.ButtonCancelar.Image = global::CRUDListaObj.Properties.Resources.Cancelar;
            this.ButtonCancelar.Location = new System.Drawing.Point(632, 57);
            this.ButtonCancelar.Name = "ButtonCancelar";
            this.ButtonCancelar.Size = new System.Drawing.Size(61, 33);
            this.ButtonCancelar.TabIndex = 6;
            this.ButtonCancelar.UseVisualStyleBackColor = true;
            this.ButtonCancelar.Click += new System.EventHandler(this.ButtonCancelar_Click);
            // 
            // ButtonGravar
            // 
            this.ButtonGravar.Enabled = false;
            this.ButtonGravar.Image = global::CRUDListaObj.Properties.Resources.Confirmar;
            this.ButtonGravar.Location = new System.Drawing.Point(547, 57);
            this.ButtonGravar.Name = "ButtonGravar";
            this.ButtonGravar.Size = new System.Drawing.Size(64, 32);
            this.ButtonGravar.TabIndex = 5;
            this.ButtonGravar.UseVisualStyleBackColor = true;
            this.ButtonGravar.Click += new System.EventHandler(this.ButtonGravar_Click);
            // 
            // ButtonEditar
            // 
            this.ButtonEditar.Image = global::CRUDListaObj.Properties.Resources.Editar;
            this.ButtonEditar.Location = new System.Drawing.Point(547, 12);
            this.ButtonEditar.Name = "ButtonEditar";
            this.ButtonEditar.Size = new System.Drawing.Size(146, 39);
            this.ButtonEditar.TabIndex = 4;
            this.ButtonEditar.UseVisualStyleBackColor = true;
            this.ButtonEditar.Click += new System.EventHandler(this.ButtonEditar_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(744, 342);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "FormPrincipal";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox ComboBoxListaAlunos;
        private System.Windows.Forms.ComboBox ComboBoxNomes;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox ComboBoxProcurar;
        private System.Windows.Forms.Button ButtonProcurar;
        private System.Windows.Forms.TextBox TextBoxProcurar;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button ButtonCancelar;
        private System.Windows.Forms.Button ButtonGravar;
        private System.Windows.Forms.Button ButtonEditar;
        private System.Windows.Forms.TextBox TextBoxApelidoAluno;
        private System.Windows.Forms.TextBox TextBoxNomeAluno;
        private System.Windows.Forms.TextBox TextBoxPrimeiroNome;
        private System.Windows.Forms.TextBox TextBoxIdAluno;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem alunoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem novoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem apagarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem actualizarToolStripMenuItem;
    }
}

